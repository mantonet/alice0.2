package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController

object QuestionsScenario: Scenario() {
    init {
        state("questions") {
            action {
                println("IN questions")
            }

            state("FAQ") {
                activators {
                    intent(AliceIntent.HELP)
                }

                action {
                    reactions.run {
                        say("WE ARE IN FAQ")
                        go("../../main/from_faq/")
                    }
                }
            }

            state("fallback"){
                activators { catchAll() }

                action {
                    val path = OurdataController(context)
                    reactions.run{
                        if(path.dummy == 0) {
                            say("Немного не понимаю вашего вопроса. Пожалуйста, повторите")
                            path.dummy++
                        }
                        else {
                            path.dummy = 0
                            go("../../main/from_faq/")
                        }
                    }
                }
            }
        }
    }
}