package com.justai.jaicf.template.scenario

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import kotlinx.coroutines.runBlocking

//import io.ktor.client.*
//import io.ktor.client.engine.cio.*
//import io.ktor.client.request.*
import com.justai.jaicf.activator.catchall.CatchAllActivator
import com.justai.jaicf.activator.catchall.catchAll
import com.justai.jaicf.channel.yandexalice.model.AliceEvent
import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.channel.yandexalice.activator.*
import com.justai.jaicf.channel.yandexalice.*
import com.justai.jaicf.context.ActionContext
//import com.justai.jaicf.api.BotRequest
import com.justai.jaicf.hook.BotHookException
import com.justai.jaicf.hook.*
import com.justai.jaicf.model.activation.Activation
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.StringReader

object MainScenario: Scenario(
        dependencies = listOf(
                QuestionsScenario,
                ConfirmScenario,
                ProffieldScenario,
                AnyScenario,
                CorpDepartmentProjectsScenario,
                TechBusinessmanProjectsScenario,
                StartupProjectsScenario,
                PositiveScenario,
                NegativeScenario,
                EndScenario)
) {
    init {
//        var interrupt = 0
        state("main") {
            activators {
                event(AliceEvent.START)
            }

            action {
//                GlobalScope.launch {
//                    repeat(1000) {
//                        interrupt++
//                        println("I'm sleeping $interrupt ...")
//                        delay(2000L)
//                    }
//                }
                println("IN MAIN STATE")
                //println("REQUEST IS " + request.input)
                val path = OurdataController(context)
                path.get_excel_page(0)
                path.initializeOurdata()
                reactions.go("findpath")
            }

            MainScenario.state("findpath") {
                action {
                    println("IN FINDPATH REQUEST IS " + request.input)
                    val path = OurdataController(context)
                    path.setOurdata()
                    if (path.qf == 0) {
                        reactions.go("../fraze")
                    } else {
                        reactions.run {
                            sayRandom(path.rep1 as String, path.rep2 as String)
                            go("../../" + path.scen)
                        }
                    }
                }
            }

            MainScenario.state("fraze") {
                action {
                    val path = OurdataController(context)
                    path.change_row(0, true)
                    //path.row = path.goother as Int - 1
                    println("IN FRAZE")
                    reactions.run {
                        sayRandom(path.rep1 as String, path.rep2 as String)
                        go("../findpath")
                    }
                }
            }

            MainScenario.state("from_faq") {
                action {
                    val path = OurdataController(context)
                    println("RETURNED FROM FAQ")
                    path.setOurdata()
                    if (path.qf == 0) {
                        reactions.go("../fraze")
                    } else {
                        reactions.run {
                            say(path.rep3 as String)
                            go("../../" + path.scen)
                        }
                    }
                }
            }
        }

        fallback {
            reactions.say("Не совсем поняла вас")
        }
    }
}

//suspend fun ActionContext.delayed_findpath(): Unit {
//    delay(5000L)
//    reactions.go("../findpath")
//}

fun ActionContext.todude() {
    //reactions.say("Going to dude")
    reactions.go("/dude")
}