package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.activator.alice
import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController
import kotlinx.serialization.json.int

object StartupProjectsScenario: Scenario() {
    init {
        state("startup_projects") {
            action {
                println("IN startup_projects")
            }

            state("projects"){
                activators { intent("PROJECTS") }

                action {
                    activator.alice?.run {
                        val project = slots["project"]
                        val path = OurdataController(context)
                        println("TOKEN IS " + project?.value)
                        when(project?.value!!.int){
                            1, 2, 4, 7 -> reactions.run {
                                say("Не совсем поняла вас. Выберите интересное вам направление")
                                go("../startup_projects")
                            }
                            5 -> path.change_row(0, false)
                            6 -> path.change_row(1, false)
                            3 -> path.change_row(2, false)
                            8 -> path.change_row(5, false)
                            9 -> path.change_row(6, false)
                        }
                        reactions.go("../../main/findpath")
                    }
                }
            }

            state("fallback") {
                activators { catchAll() }

                action { reactions.say("Не совсем поняла вас. Выберите интересное вам направление") }
            }
        }
    }
}