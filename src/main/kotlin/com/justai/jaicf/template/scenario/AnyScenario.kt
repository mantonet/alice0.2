package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController

object AnyScenario: Scenario() {

    init {
        state("any") {
            action {
                println("IN ANY")
            }

            state("catchall") {
                activators { catchAll() }

                action {
                    val path = OurdataController(context)
                    path.change_row(0, true)
                    reactions.go("../../main/findpath")
                }
            }
        }
    }
}