package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController

object ConfirmScenario: Scenario() {
    init {
        state("confirm") {
            action {
                println("IN CONFIRM")
            }

            state("yep")
            {
                activators {
                    intent(AliceIntent.CONFIRM)
                }

                action {
                    val path = OurdataController(context)
                    println("IN YEP")
                    path.change_row(0, false)
                    //path.row = path.go[0].toInt() - 1
                    //context.session["ourdata"] = path
                    println("Going to " + (path.row as Int + 1) + " row")
                    reactions.go("../../main/findpath")
                }
            }

            state("nope")
            {
                activators {
                    intent(AliceIntent.REJECT)
                    intent("NEGATIVE")
                }

                action {
                    val path = OurdataController(context)
                    println("IN NOPE")
                    path.change_row(0, true)
                    //path.row = path.goother as Int - 1
                    println("Going to " + (path.row as Int + 1) + " row")
                    reactions.go("../../main/findpath")
                }
            }

            fallback {
                val path = OurdataController(context)
                if (path.dummy == 0) {
                    path.dummy++
                    reactions.say("Не совсем поняла вас, пожалуйста повторите")
                }
                else {
                    path.dummy = 0
                    reactions.run {
                        say("Мне трудно понять вас, может быть у вас есть вопросы?")
                        go("../../questions/")
                    }
                }
//                println("going back to main state")
//                reactions.changeState("../../main")
            }
        }
    }
}

//fun ActionContext.confirm(message: String, callback: String) {
//    reactions.say(message)
//    reactions.changeState("/record", callback)
//}