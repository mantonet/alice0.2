package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.activator.alice
import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController
import kotlinx.serialization.json.int

object TechBusinessmanProjectsScenario: Scenario() {
    init {
        state("tech_businessman_projects") {
            action {
                println("IN tech_businessman_projects")
            }

            state("projects"){
                activators { intent("PROJECTS") }

                action {
                    activator.alice?.run {
                        val project = slots["project"]
                        val path = OurdataController(context)
                        println("TOKEN IS " + project?.value)
                        when(project?.value!!.int){
                            2, 7 -> reactions.run {
                                say("Не совсем поняла вас. Выберите интересное вам направление")
                                go("../tech_businessman_projects")
                            }
                            3 -> path.change_row(0, false)
                            1 -> path.change_row(1, false)
                            4 -> path.change_row(2, false)
                            5 -> path.change_row(3, false)
                            6 -> path.change_row(4, false)
                            8 -> path.change_row(5, false)
                            9 -> path.change_row(6, false)
                        }
                        reactions.go("../../main/findpath")
                    }
                }
            }

            state("fallback") {
                activators { catchAll() }

                action { reactions.say("Не совсем поняла вас. Выберите интересное вам направление") }
            }
        }
    }
}