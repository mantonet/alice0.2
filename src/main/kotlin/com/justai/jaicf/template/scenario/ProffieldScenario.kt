package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController
import com.justai.jaicf.channel.yandexalice.activator.alice
import kotlinx.serialization.json.int

object ProffieldScenario: Scenario() {

    init {
        state("proffield") {
            action {
                println("IN PROFFIELD")
            }

            state("proffield_check"){
                activators { intent("PROFFIELD.CHECK") }

                action {
                    activator.alice?.run {
                        val prof_is = slots["prof_is"]
                        val path = OurdataController(context)
                        println("TOKEN IS " + prof_is?.value)
                        path.get_excel_page(prof_is?.value!!.int)
                        path.initializeOurdata()
                        reactions.go("../../main/findpath")
                    }
                }
            }

            state ("fallback") {
                activators { catchAll() }

                action {
                    val path = OurdataController(context)
                    if (path.dummy == 0) {
                        path.dummy++
                        reactions.say("Не совсем поняла вас, пожалуйста повторите")
                    }
                    else {
                        path.dummy = 0
                        reactions.run {
                            say("Мне трудно понять вас, может быть у вас есть вопросы?")
                            go("../../questions/")
                        }
                    }
                }
            }
        }
    }
}