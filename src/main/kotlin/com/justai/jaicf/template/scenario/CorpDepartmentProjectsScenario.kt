package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.activator.alice
import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController
import kotlinx.serialization.json.int

object CorpDepartmentProjectsScenario: Scenario() {

    init {
        state("corp_department_projects") {
            action {
                println("IN corp_department_projects")
            }

            state("projects"){
                activators { intent("PROJECTS") }

                action {
                    activator.alice?.run {
                        val project = slots["project"]
                        val path = OurdataController(context)
                        println("TOKEN IS " + project?.value)
                        when(project?.value!!.int){
                            1, 2, 3, 5, 7 -> reactions.run {
                                say("Не совсем поняла вас. Выберите интересное вам направление")
                                go("../corp_department_projects")
                            }
                            4 -> path.change_row(0, false)
                            6 -> path.change_row(1, false)
                            8 -> path.change_row(2, false)
//                            4 -> path.row = path.go[0].toInt() - 1
//                            6 -> path.row = path.go[1].toInt() - 1
//                            8 -> path.row = path.go[2].toInt() - 1
                        }
                        reactions.go("../../main/findpath")
                    }
                }
            }

            state("fallback") {
                activators { catchAll() }

                action { reactions.say("Не совсем поняла вас. Выберите интересное вам направление") }
            }
        }
    }
}