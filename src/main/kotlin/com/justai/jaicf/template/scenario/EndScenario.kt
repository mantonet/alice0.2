package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.alice
import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController

object EndScenario: Scenario() {

    init {
        state("END") {
            action {
                println("IN END")
                reactions.alice?.endSession()
            }
        }
    }
}