package com.justai.jaicf.template.scenario

import com.justai.jaicf.channel.yandexalice.model.AliceIntent
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.model.OurdataController

object PositiveScenario: Scenario() {
    init {
        state("positive") {
            action {
                println("IN POSITIVE")
            }

            state("negative")
            {
                activators {
                    intent("NEGATIVE")
                }

                action {
                    val path = OurdataController(context)
                    println("GOT NEGATIVE RESPONSE IN POSITIVE STATE")
                    path.change_row(0, true)
                    //path.row = path.goother as Int - 1
                    println("Going to " + (path.row as Int + 1) + " row")
                    reactions.go("../../main/findpath")
                }
            }

            fallback {
                val path = OurdataController(context)
                println("LOOKS LIKE POSITIVE RESPONSE IN POSITIVE STATE")
                path.change_row(0, false)
                //path.row = path.go[0].toInt() - 1
                println("Going to " + (path.row as Int + 1) + " row")
                reactions.go("../../main/findpath")
            }
        }
    }
}