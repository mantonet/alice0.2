package com.justai.jaicf.template.model

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.justai.jaicf.context.BotContext
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import kotlinx.coroutines.runBlocking
import java.io.StringReader
import java.net.http.HttpClient

data class Go(val page : Int, val row : Int)

class OurdataController(context: BotContext){
    var rep1 : String by context.client
    var rep2 : String by context.client
    var rep3 : String by context.client
    var row : Int by context.client
    var scen : String by context.client
    var go : MutableList<Go> by context.client
    var goother : Go by context.client
    var qf : Int by context.client
    var page_uri : String by context.client
    var parsed_page : JsonArray<*> by context.client
    var proffield_index : Int by context.client
    var dummy : Int by context.client

    val httpClient = HttpClient(CIO)
    val klaxon = Klaxon()

//    val intro_page = "http://tools.aimylogic.com/api/googlesheet2json?sheet=2&id=1H62r3adzZ9KEVzjnGKFTV3UtivNaUzXMOhEKa_GSPeg"
//    val students_page = "http://tools.aimylogic.com/api/googlesheet2json?sheet=3&id=1H62r3adzZ9KEVzjnGKFTV3UtivNaUzXMOhEKa_GSPeg"
//    val startup_page = "http://tools.aimylogic.com/api/googlesheet2json?sheet=3&id=1H62r3adzZ9KEVzjnGKFTV3UtivNaUzXMOhEKa_GSPeg"
//    val entrepreneur_page = "http://tools.aimylogic.com/api/googlesheet2json?sheet=4&id=1H62r3adzZ9KEVzjnGKFTV3UtivNaUzXMOhEKa_GSPeg"
//    val corporation_page = "http://tools.aimylogic.com/api/googlesheet2json?sheet=5&id=1H62r3adzZ9KEVzjnGKFTV3UtivNaUzXMOhEKa_GSPeg"
//    val city_page = "http://tools.aimylogic.com/api/googlesheet2json?sheet=6&id=1H62r3adzZ9KEVzjnGKFTV3UtivNaUzXMOhEKa_GSPeg"

    fun get_excel_page(proff_ind : Int) {
        proffield_index = proff_ind
        page_uri = "http://tools.aimylogic.com/api/googlesheet2json?sheet=" + (proffield_index + 2) + "&id=1H62r3adzZ9KEVzjnGKFTV3UtivNaUzXMOhEKa_GSPeg"
        val reply = runBlocking {
            httpClient.get<String>(page_uri)
        }
        parsed_page = klaxon.parseJsonArray(StringReader(reply))
    }

    fun question_or_fraze() {

    }

    fun setOurdata() {
        val jsnObj = parsed_page.get(row) as JsonObject

        if (jsnObj.get("qf") is Int) qf = jsnObj.get("qf") as Int
        rep1 = jsnObj.get("rep1") as String
        rep2 = jsnObj.get("rep2") as String
        if (jsnObj.get("rep3") != "") { rep3 = jsnObj.get("rep3") as String }
        if (jsnObj.get("goother").toString() != "") { goother = get_one_go(jsnObj.get("goother").toString()) }
        //println("GO IN FUN IS" + jsnObj.get("go") + " AND THE ROW IS " + row)
        if (jsnObj.get("go").toString() != "") { go = get_go(jsnObj.get("go").toString()) }
        println(go[0])
        scen = jsnObj.get("scenario") as String
    }

    fun get_go(raw_go : String): MutableList<Go> {
        /**
         Breaks down one string containting multiple paths
         into multiple "Go" data structures. String example: 1 2:8 3 4
         If excel page is specified the following way:
         1:24, we will get path (page:1, row: 24)
         */
        val paths: List<String> = raw_go.split(" ")
        val list_go : MutableList<Go> = ArrayList()
        var i : Int
        if (paths.size > 0)
        for (one_true_path : String in paths)
        {
            list_go.add(get_one_go(one_true_path))
        }
        return (list_go)
    }

    fun get_one_go(one_true_path : String): Go {
        /**
        Breaks down one "go" string into path for "Go" data structure.
        If excel page is specified the following way:
        1:24, we will get path (page:1, row: 24)
         */
        val buff : List<String>
        var go_construct = Go(0, 0)
        buff = one_true_path.split(":")
        if (buff.size == 1) {
            go_construct = Go (proffield_index, buff[0].toInt())
        }
        else if (buff.size == 2) {
            go_construct = Go (buff[0].toInt(), buff[1].toInt())
        }
        else {go_construct = Go (0, 0)}
        return (go_construct)
    }

    fun change_row(go_struct_number : Int, using_goother : Boolean) {
        if (using_goother == false) {
            if (go[go_struct_number].page == proffield_index) {
                row = go[go_struct_number].row - 1
            } else {
                get_excel_page(go[go_struct_number].page)
                row = go[go_struct_number].row - 1
            }
        }
        else {
            if (goother.page == proffield_index) {
                row = goother.row - 1
            } else {
                get_excel_page(goother.page)
                row = goother.row - 1
            }
        }
    }

    fun send_command() {
        val reply = runBlocking {
            httpClient.post<String>(page_uri)
        }
    }

    fun manual_change_row(row_number : Int, prof_ind : Int) {
        get_excel_page(prof_ind)
        row = row_number
    }

    fun initializeOurdata() {
        row = 0
        dummy = 0
        setOurdata()
    }
}

