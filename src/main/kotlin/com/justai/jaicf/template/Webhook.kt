package com.justai.jaicf.template

import com.justai.jaicf.channel.http.httpBotRouting
import com.justai.jaicf.channel.yandexalice.AliceChannel
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import com.beust.klaxon.*
//import com.justai.jaicf.template.scenario.httpClient
import io.ktor.client.request.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.io.StringReader

data class User(val name: String, val age: Int)

fun main() {
    embeddedServer(Netty, System.getenv("PORT")?.toInt() ?: 8080) {
        routing {
            httpBotRouting("/" to AliceChannel(
                skill2,
                System.getenv("OAUTH_TOKEN") ?: "AgAAAABEDQOdAAT7o1yArkdpr0hQnym6vtsDdy0",)
 //                   useDataStorage = true),
            )
        }
    }.start(wait = true)
}